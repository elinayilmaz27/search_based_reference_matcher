package org.crossref.refmatching;

import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;
import org.crossref.common.utils.ResourceUtils;
import org.json.JSONArray;
import org.json.JSONObject;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

/**
 *
 * @author Dominika Tkaczyk
 */
public class CandidateValidatorTest {    
    private static final String TEST_FILE =
            "/api-responses/unstructured-ref-response-1.json";
    
    List<Candidate> candidates;
    
    CandidateValidator validator = new CandidateValidator();
    
    @Before
    public void setup() {
        String content = ResourceUtils.readResourceAsString(TEST_FILE);
        JSONArray json = new JSONObject(content).getJSONObject("message")
                .optJSONArray("items");
        candidates = 
                StreamSupport.stream(json.spliterator(), false).map(
                    item -> new Candidate((JSONObject) item))
                .collect(Collectors.toList());
    }
    
    @Test
    public void testChooseCandidate() {
        Candidate chosen = validator.chooseCandidate(new Reference(
                "Tkaczyk, D., Tarnawski, B., & Bolikowski, Ł. (2015). "
                + "Structured Affiliations Extraction from Scientific "
                + "Literature. D-Lib Magazine, 21(11/12)."), candidates, 0.9);
        Assert.assertEquals(null, chosen);
        
        chosen = validator.chooseCandidate(new Reference(
                "Tkaczyk, D., Tarnawski, B., & Bolikowski, Ł. (2015). "
                + "Structured Affiliations Extraction from Scientific "
                + "Literature. D-Lib Magazine, 21(11/12)."), candidates, 0.5);
        Assert.assertEquals(candidates.get(2), chosen);
        Assert.assertEquals(0.857611, chosen.getValidationScore(), 0.0001);
    }
    
    @Test
    public void testGetRescoredCandidates() {
        List<Candidate> rescored = validator.getRescoredCandidates(new Reference(
                "Tkaczyk, D., Tarnawski, B., & Bolikowski, Ł. (2015). "
                + "Structured Affiliations Extraction from Scientific "
                + "Literature. D-Lib Magazine, 21(11/12)."), candidates, 0.10);
        Assert.assertEquals(5, rescored.size());
        Assert.assertEquals(0.857611, rescored.get(0).getValidationScore(),
                0.0001);
    }
    
}