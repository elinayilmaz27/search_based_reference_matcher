package org.crossref.refmatching;

import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;

/**
 * Class for selecting a single target document from a list of candidates.
 * 
 * @author Dominika Tkaczyk
 */
public class CandidateValidator {

    public Candidate chooseCandidate(Reference reference,
            List<Candidate> candidates, double minScore) {
        candidates = getRescoredCandidates(reference, candidates, minScore);
        
        return (!candidates.isEmpty() &&
                candidates.get(0).getValidationScore() >= minScore)
                ? candidates.get(0) : null;
    }
    
    public List<Candidate> getRescoredCandidates(Reference reference,
            List<Candidate> candidates, double minScore) {
        candidates.forEach(c ->
            {c.setValidationScore(
                    c.getValidationSimilarity(reference, minScore));});
        candidates = candidates.stream()
                .filter(c -> c.getValidationScore() > 0)
                .collect(Collectors.toList());
        Collections.sort(
                candidates, 
                (c1, c2) -> Double.compare(c2.getValidationScore(),
                                           c1.getValidationScore()));
        return candidates;
    }

}